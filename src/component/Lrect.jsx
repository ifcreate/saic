import React, { Component } from 'react'
import Slider from './Slider.jsx';
import '../less/Lrect.less'
export default class Lrect extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        const text = this.props.text;
        const imgcls = text == '天气好' ? ' weather'
            : text == '有朋友' ? ' friends'
                : text == '心情好' ? ' mood' : '';
        return (
            <div className="stage1-wrapper">
                <div className={`rect1${imgcls}`}>
                    <div className='text-describe'>
                        <div className='div-text'>{this.props.text}</div>
                    </div>
                </div>
                <div className='rect2'
                     style={{
                         backgroundColor: this.props.inputColor || this.props.sliderInfo.colorLeft,
                     }}
                />
                <div className='line1'
                     style={{
                         transform: `rotateZ(${this.props.deg}deg)`,
                         width: this.props.width + 'px',
                     }}
                >
                    <Slider
                        pos={this.props.pos}
                        sliderInfo={this.props.sliderInfo}
                        width={this.props.width}
                        transitOn={this.props.transitOn}
                    />
                </div>
                {
                    this.props.sliderInfo1 &&
                    <div className='line1'
                         style={{
                             transform: `rotateZ(${this.props.deg1}deg)`,
                             width: this.props.width1 + 'px',
                         }}
                    >
                        <Slider
                            pos={this.props.pos}
                            sliderInfo={this.props.sliderInfo1}
                            width={this.props.width1}
                            transitOn={this.props.transitOn}
                        />
                    </div>
                }
                {
                    this.props.sliderInfo2 &&
                    <div className='line1'
                         style={{
                             transform: `rotateZ(${this.props.deg2}deg)`,
                             width: this.props.width2 + 'px',
                         }}
                    >
                        <Slider
                            pos={this.props.pos}
                            sliderInfo={this.props.sliderInfo2}
                            width={this.props.width2}
                            transitOn={this.props.transitOn}
                        />
                    </div>
                }
            </div>
        )
    }
}
