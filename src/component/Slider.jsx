import React, { Component } from 'react'
import '../less/Slider.less'
const trans_time = '1s';

const maxH = 25;
const minH = 3;

export default class Lrect extends Component {
    /*constructor(props) {
        super(props);
    }*/
    getH(r) {
        return Math.max(minH, r * maxH);
    }
    render() {
        const sliderInfo = this.props.sliderInfo || {};
        return (
            <div className='slider' style={{width: this.props.width + 'px'}}>
                <div className="s-left s-trans"
                     style={{
                         width: `${this.props.pos}%`,
                         height: `${this.getH(sliderInfo.weightLeft)}px`,
                         transition: this.props.transitOn ? `width ${trans_time} linear` : 'none',
                         backgroundColor: sliderInfo.colorLeft || sliderInfo.colorDot,
                     }}
                />
                <div className="s-dot s-trans"
                     style={{
                         left: `${this.props.pos}%`,
                         transition: this.props.transitOn ? `left ${trans_time} linear` : 'none',
                         backgroundColor: sliderInfo.colorDot || sliderInfo.colorLeft,
                     }}
                />
                <div className="s-right s-trans"
                     style={{
                         width: `${100 - this.props.pos}%`,
                         height: `${this.getH(sliderInfo.weightRight)}px`,
                         transition: this.props.transitOn ? `width ${trans_time} linear` : 'none',
                         backgroundColor: sliderInfo.colorRight,
                     }}
                />
            </div>
        )
    }
}
