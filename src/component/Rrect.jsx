import React,{Component} from 'react'
import '../less/Rrect.less'
import Anselect from './Anselect.jsx';
export default class Rrect extends Component
{
    constructor(props){
        super(props)
        this.state = {
            value:0,
        }
    }
    render(){
        const text = this.props.text;
        const imgcls = (text == '去看电影'|| text=='看电影') ? ' film'
            : text == '打球' ? ' ball'
                : text == '看书' ? ' read'
                    : '';
        return(
            <div className={'center-right'+(this.props.rowIdx==0? ' setTop' : '')}>
                <div className={`rrect${imgcls}`}>
                    {
                        (this.props.rowIdx == 0 || this.props.rowIdx == 1) && <p className="title-res">Robi的判断</p>
                    }
                    <div className='res-text'>{this.props.text}</div>
                    <div className='rsrect'
                         style={{
                             backgroundColor: this.props.resColor,
                         }}
                    >
                        {
                            this.props.popon && <Anselect
                                closePop={this.props.closePop}
                                type={this.props.rowIdx}
                            />
                        }
                    </div>
                </div>

            </div>
        )
    }
}
