import React,{Component} from 'react'
import '../less/Anselect.less'
export default class Rrect extends Component
{
    constructor(props){
        super(props)
        this.state = {
            value:0,
            yesIndex: -1,
        }
    }
    select(i) {
        this.setState({
            yesIndex: i,
        })
        this.props.closePop(i);
    }
    render(){
        if (this.props.type == 1) { // single neural
            return (
                <div className='an-rect'>
                    <div className='tbcircle'>
                        <div className='lcircle'></div>
                        <div className='rcircle'></div>
                    </div>
                    <div className='q-title'>
                        <div className='div-text'>请选择正确答案</div>
                    </div>
                    <div className='btn' onClick={() => this.props.closePop(1)}>去</div>
                    <div className='btn bt-no' onClick={() => this.props.closePop()}>不去</div>
                    <div className='tbcircle'>
                        <div className='lcircle'></div>
                        <div className='rcircle'></div>
                    </div>
                </div>
            )
        } else if (this.props.type == 0) {  //neuralnet
            return (
                <div className='an-rect an-rect1'>
                    <div className='tbcircle'>
                        <div className='lcircle'></div>
                        <div className='rcircle'></div>
                    </div>
                    <div className='q-title'>
                        <div className='div-text'>请选择正确答案</div>
                    </div>
                    {
                        ['去看电影','去打球', '去看书'].map((item,i) => {
                            return (
                                <div
                                    className={`btn1${this.state.yesIndex == i ? ' yes' : ''}`}
                                    onClick={() => this.select(i)}
                                >{item}</div>
                            )
                        })
                    }
                    <div className='tbcircle'>
                        <div className='lcircle'></div>
                        <div className='rcircle'></div>
                    </div>
                </div>
            )
        } else {
            return null
        }
    }
}
