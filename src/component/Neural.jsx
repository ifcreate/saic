import React, { Component } from 'react'
import Slider from './Slider.jsx';
import '../less/Neural.less'
export default class Neural extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <div className='class-center-center'>
                {
                    this.props.index == 0 && <p className="title-circle">神经元</p>
                }
                <div className='ccircle'/>
                <div className='center-line'>
                    <div className='line2'
                         style={{
                             transform: `rotateZ(${this.props.deg}deg)`,
                             width: this.props.width + 'px',
                         }}
                    >
                        <Slider
                            pos={this.props.pos}
                            transitOn={this.props.transitOn}

                            sliderInfo={this.props.sliderInfo}
                            width={this.props.width}
                        />
                    </div>
                    {
                        this.props.sliderInfo1 &&
                        <div className='line2'
                             style={{
                                 transform: `rotateZ(${this.props.deg1}deg)`,
                                 width: this.props.width1 + 'px',
                             }}
                        >
                            <Slider
                                pos={this.props.pos}
                                transitOn={this.props.transitOn}

                                sliderInfo={this.props.sliderInfo1}
                                width={this.props.width1}
                            />
                        </div>
                    }
                    {
                        this.props.sliderInfo2 &&
                        <div className='line2'
                             style={{
                                 transform: `rotateZ(${this.props.deg2}deg)`,
                                 width: this.props.width2 + 'px',
                             }}
                        >
                            <Slider
                                pos={this.props.pos}
                                transitOn={this.props.transitOn}

                                sliderInfo={this.props.sliderInfo2}
                                width={this.props.width2}
                            />
                        </div>
                    }
                </div>
            </div>
        )
    }
}
