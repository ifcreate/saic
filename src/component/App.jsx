import React, { Component } from 'react';
import '../less/App.less';

export default class app extends Component {
    constructor(props) {
        super(props);
        this.state = {
            map: '', //
            hi: '',
            weather: '',
            schedule: '',
            welcomeBg: '',
            fade: '',
            search: '',
        };
    }
    componentDidMount() {
        this.startDemo();
    }
    startDemo() {
        this.fucgen('hi');
        this.fucgen('weather');
        this.fucgen('schedule');
        this.fucgen('welcomeBg');
        this.fucgen('fade');
        this.fucgen('search');
        setTimeout(() => {
            this.map('mr500');
            this.search('right1069');
        }, 2000);

        setTimeout(() => {
            this.hi('hi-down');
            this.welcomeBg('in');
            this.fade('in');
        }, 2200);

        setTimeout(() => {
            this.weather('weather-right');
        }, 2400);

        setTimeout(() => {
            this.schedule('schedule-up');
        }, 2600);

        // next page
        setTimeout(() => {
            this.welcomeBg('w519');
            this.search('left1069to534');
            this.map('mr250')
            this.hi('topneg238');
        }, 4600);
        setTimeout(() => {
            this.weather('leftneg1069');
        }, 4800);
    }
    fullScreen = () => {
        this.startDemo();
        var docElm = document.documentElement;
        if (docElm.requestFullscreen) {
            docElm.requestFullscreen();
        } else if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen();
        } else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen();
        } else if (docElm.msRequestFullscreen) {
            docElm.msRequestFullscreen();
        }
    }
    toleft = () => { //map to left
        this.setState({
            map: 'mapleft1',
        })
    }
    map(s) {
        this.setState({
            map: s,
        })
    }
    fucgen(name) {
        this[name] = (s) => {
            this.setState({
                [name]: s,
            })
        }
    }
    render() {
        return (
            <div id='App'>
                <div className="left-bar"></div>
                <div className="bottom-bar"></div>
                <div className={ "map" + " " + this.state.map }>
                    <div className={ "search" + " " + this.state.search }></div>

                    <div className={ "welcome-bg" + " " + this.state.welcomeBg }>
                        <div className="bgmain">

                            <div className="hline"></div>
                            <div className="vline"></div>
                            <div className={ "hi" + " " + this.state.hi }>
                                <p className="hi-text">Hi, 早上好！<br />
                                    周一文。
                            </p>
                                <p className="hi-date">2019/05/07 今天</p>
                            </div>
                            <div className={ "weather" + " " + this.state.weather }>
                                <div className="title"></div>
                                <div className="report"></div>
                                <div className="date">
                                    今天
                                <span className="round">Mon</span>
                                    <span className="time">17:26</span>
                                </div>
                                <div className="icons">
                                    {/* <div className="col">
                                    <span className="icon i1"></span>湿度<span className="notes">75%</span>
                                </div>
                                <div className="col">
                                    <span className="icon i2"></span>空气质量<span className="notes">轻度污染</span>
                                    </div>
                                    <div className="col">
                                    <span className="icon i3"></span>风力风向<span className="notes">东风2级</span>
                                    </div>
                                    <div className="col">
                                    <span className="icon i4"></span>紫外线<span className="notes">较弱</span>
                                </div> */}
                                </div>
                            </div>
                            <div className={ "schedule" + " " + this.state.schedule }>
                                <div className="title"></div>
                                <div className="task task1"></div>
                                <div className="task task2"></div>
                                <div className="task task3"></div>
                                <div className="task others"></div>
                            </div>
                        </div>
                        <div className={ "fade" + " " + this.state.fade }></div>
                    </div>

                    <button className="full" onClick={ this.fullScreen }>全屏</button>
                </div>
            </div>
        )
    }
}