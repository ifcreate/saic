
// ------------ Create a simple model_multi. ----------------

const model_multi = window.tf.sequential();

model_multi.add(window.tf.layers.dense({
    inputShape: [3],
    units: 3,
    kernelInitializer: 'ones',
    activation: 'softmax',
    useBias: true
}));


// Prepare the model_multi for training: Specify the loss and the optimizer.
let adam_multi = window.tf.train.adam(0.1);
model_multi.compile({loss: 'categoricalCrossentropy', optimizer: adam_multi});

// console.log(model_multi.summary());


// -------- main functions --------

export async function train_multi(x, y) {
    // Generate some synthetic data for training.
    const xs = window.tf.tensor2d(x);
    const ys = window.tf.tensor2d(y);

    // Train the model_multi using the data.
    return model_multi.fit(xs, ys, {batchSize: 1, epochs: 10});
}


export async function predict_multi(x) {
    // Use the model_multi to do inference on a data point the model_multi hasn't seen.
    // Should print approximately 39.

    // document.getElementById('inputs-div').innerText = x ;

    // document.getElementById('predict-out-div').innerText =
        model_multi.predict(window.tf.tensor2d(x, [1, 3])).dataSync();

    let output = index_of_max(model_multi.predict(window.tf.tensor2d(x, [1, 3])).dataSync());
    // let weights = get_network_weights_multi();

    // document.getElementById('predict-out-div2').innerText = output;

    return output;
}


export async function get_network_weights_multi() {
    // weights
    // 格式：Wij （表示第 i 层神经元连接到第 j 层神经元的权重）
    // [w11,  w12, w13, w21, w22, ..., w33]
    //  0 1 2, 3 4 5, 6 7 8.

    const weights = model_multi.getWeights();
    const promise0 = weights[0].data();
    const p1 = promise0.then(function(value) {
        value = normalize_array(value, 5, -3);
        // document.getElementById('weights1-out-div').innerText = value;
        return value;
    });

    return p1;
}


export async function click_random_button_multi() {
    const x = randomArray();
    predict_multi(x);
}


export async function click_correct_answer_multi(index) {
    const inputs = document.getElementById('inputs-div').innerText;
    const x = get_input_array(inputs);
    const y = get_one_hot_array(index);

    console.log('Prepare to train_multi with the data : \n', 'x:', x, ',\t', 'y:', y);
    await train_multi(x, y);
    await get_network_weights_multi();
    console.log('Training successfully!');
}


// -------- utils -----------

export let get_one_hot_array = (index) => {
    let arr = [[0,0,0]];
    arr[0][index] = 1;

    return arr
};


function index_of_max(a) {
    return a.indexOf(Math.max.apply(Math, a));
}


function get_input_array(str) {
    let dataIntArr = [];
    const dataStrArr = str.split(",");//分割成字符串数组
    dataStrArr.forEach(function(data){
        dataIntArr.push(+data);
    });

    return [dataIntArr]
}


let randomArray = (length) => {
    let arr = [length];
    length = typeof(length) === 'undefined' ? 3 : length;
    for(let i=1; i<=length; i++) {
        arr[i-1] = Math.round(Math.random())
    }

    return arr
};


function normalize_array(arr, max, min) {
    // Min-Max Normalization
    // x' = (x - min) / (max - min)
    for (let i=0; i<arr.length; i++) {
        if (arr[i] < min) {
            arr[i] = min
        } else if (arr[i] > max) {
            arr[i] = max
        }

        arr[i] = (arr[i] - min) / (max - min);
    }

    return arr
}
