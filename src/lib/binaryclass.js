
// ------------ Create a simple model. ----------------

export const model = window.tf.sequential();

model.add(window.tf.layers.dense({
    inputShape: [3],
    units: 1,
    kernelInitializer: 'ones',
    activation: 'sigmoid',
    useBias: true
}));


// model.add(window.tf.layers.dense({
//     units: 1,
//     kernelInitializer: 'ones',
//     activation: 'softmax',
//     useBias: false,
// }));

// Prepare the model for training: Specify the loss and the optimizer.
let adam = window.tf.train.adam(0.1);
model.compile({loss: 'binaryCrossentropy', optimizer: adam});

// console.log(model.summary());


// -------- main functions --------

export async function train(x, y) {
    // Generate some synthetic data for training.
    const xs = window.tf.tensor2d(x);
    const ys = window.tf.tensor2d(y);

    // Train the model using the data.
    return model.fit(xs, ys, {batchSize: 1, epochs: 10});
}


export async function predict(x) {
    // Use the model to do inference on a data point the model hasn't seen.
    // Should print approximately 39.

    // document.getElementById('inputs-div').innerText = x ;

    // document.getElementById('predict-out-div').innerText =
    //     model.predict(window.tf.tensor2d(x, [1, 3])).dataSync() + '(' +
    //     Math.round(model.predict(window.tf.tensor2d(x, [1, 3])).dataSync()) + ')';

    let output = (model.predict(window.tf.tensor2d(x, [1, 3])).dataSync());
    // let weights = get_network_weights();

    return output;
}


export async function get_network_weights(cb) {
    const weights = model.getWeights();
    const promise0 = weights[0].data();
    const p1 = promise0.then(function(value) {
        // value = normalize_array(value, 10, -5);
        value = normalize_array(value, 5, -3); // 更明显
        // cb(value);
        return value;
    });
    return p1;
}

export async function click_random_button() {
    const x = randomArray();
    const out = predict(x);

    return {'inputs': x, 'result': out['output'], 'weights': out['weights']};
}


export async function click_correct_answer(index) {
    const inputs = document.getElementById('inputs-div').innerText;
    const x = get_input_array(inputs);
    const y = [[index]];

    console.log('Prepare to train with the data : \n', 'x:', x, ',\t', 'y:', y);
    await train(x, y);
    await get_network_weights();
    console.log('Training successfully!');
}


// -------- utils -----------

export function get_input_array(str) {
    let dataIntArr = [];
    const dataStrArr = str.split(",");//分割成字符串数组
    dataStrArr.forEach(function(data){
        dataIntArr.push(+data);
    });

    return [dataIntArr]
}

export function randomArray (length) {
    let arr = [length];
    length = typeof(length) === 'undefined' ? 3 : length;
    for(let i=1; i<=length; i++) {
        arr[i-1] = Math.round(Math.random())
    }

    return arr
};


export function normalize_array(arr, max, min) {
    // Min-Max Normalization
    // x' = (x - min) / (max - min)
    for (let i=0; i<arr.length; i++) {
        if (arr[i] < min) {
            arr[i] = min
        } else if (arr[i] > max) {
            arr[i] = max
        }

        arr[i] = (arr[i] - min) / (max - min);
    }
    return arr
}


//
// console.log(predict([1,1,1]))
